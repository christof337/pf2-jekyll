---
title: "Savoir (Intelligence)"
titleEN: "Lore"
source: "Playtest Pathfinder"
toc: true

index:
  - key: "Savoir (compétence)"
    anchor: ""
  - key: "Se rappeler de connaissances"
    anchor: "se-souvenir-de-connaissances"
  - key: "Pratiquer un métier"
    anchor: "pratiquer-un-métier"

---

Vous disposez d’informations spécifiques sur un sujet précis. 
Savoir est une compétence différente des autres de deux manières.

Premièrement, la compétence Savoir comporte de nombreuses sous-catégories. Vous pourriez avoir une compétence Savoir militaire, Savoir Naval, Savoir vampire ou tout autre sorte de sous-catégorie dans cette compétence.
Chaque sous-catégorie compte comme une compétence à part entière de sorte que si vous appliquez une augmentation à la compétence Savoir planaire n’augmentera pas votre compétence Savoir naval.
Vous obtenez une sous-catégorie de la compétence grâce à votre background.
Le MJ détermine quelles sont les autres sous-catégories de la compétence qu’il autorise comme des compétences de Savoir, bien que ces catégories sont toujours moins larges que chaque autre catégorie qui vous permet de vous Remémorer des connaissances et elles ne devraient pas vous permettre de remplacer totalement une autre compétence pour vous Remémorer des connaissances. Par exemple, vous ne pouvez choisir la compétence Savoir magique pour vous Remémorer des connaissances de même type que celles couvertes par Arcanes ou Savoir aventureux pour vous permettre d'obtenir toutes les connaissances qui seraient nécessaires à un aventurier.

Deuxièmement, toutes les sous-catégories de compétences de Savoir sont des compétences de signature. Cela signifie que vous pouvez progresser pour devenir expert ou légendaire dans cette compétence pour chaque sous-catégorie.
Si vous pouvez appliquer plusieurs sous catégories dans cette compétence de Savoir pour effectuer votre test ou que vous pouvez faire se chevaucher ces sous catégories avec une autre compétence utilisable compte tenu des circonstances, vous pouvez choisir d’utiliser la compétence avec le meilleur modificateur ou celle que vous préférez utiliser. 
S’il y a le moindre doute pour savoir si une sous-catégorie de Savoir s’applique à un sujet particulier, c'est le MJ qui décide si elle peut ou non être utilisée.

## Utilisation de la compétence Savoir sans entraînement
Vous pouvez accomplir l’action suivante de la compétence Savoir, même si vous n’êtes pas entraîné.

{: .block .action1}
> ### Se rappeler de connaissances
> Vous pouvez utiliser la compétence Savoir pour vous remémorer un peu des connaissances relatives à ce type de Savoir.
> Le MJ détermine le DD pour de tels tests et si votre Savoir s’applique à ce sujet.
>
> **Réussite** Vous vous souvenez de connaissances.
>
> **Échec critique** Vous vous souvenez de connaissances erronées.

## Utilisation de la compétence Savoir avec entraînement
Vous pouvez tenter l’utilisation suivante de la compétence Savoir seulement si vous êtes entraîné dans la compétence Savoir.

> ### Pratiquer un métier
> 
> {: .traits}
> Concentration
> Secret
> Temps libre
>
> ---
>
> Vous appliquez les bénéfices pratiques de la sous-catégorie de votre Savoir pour en tirer un bénéfice durant les périodes de temps libre. Pratiquer un commerce est plus efficace avec des spécialités de Savoir comme Commerce, Lois ou Navigation où il y a de la demande pour des praticiens.
> Le MJ peut accroître le DD si vous essayez de faire commerce d’une sous-catégorie de Savoir obscure. 
> Le MJ assigne une tâche qui représente la difficulté du plus lucratif des travaux disponibles.
> Vous pouvez demander à effectuer des tâches de niveau moindre, le MJ déterminant s’il en existe une.
> Quelques fois, vous pouvez tenter de trouver des travaux de meilleur niveau que les offres initiales qui vous sont faites mais cela nécessite de dépenser du temps libre pour Recueillir des informations ou pour le rechercher et disposer des contacts.
> Lorsque vous acceptez un emploi, le MJ fixe le DD de votre compétence de Savoir. 
> Le montant de vos gains dépend de la difficulté de la tâche et de votre degré de maîtrise comme indiqué sur la table des revenus de la compétence ci-après.
> Vous pouvez avoir besoin d’outils spécialisés pour pouvoir occuper l’emploi, comme des outils de mineur dans une mine, une balance pour acheter et vendre des denrées sur un marché.
> Vous devez dépenser un nombre de jours de temps libre minimal pour trouver un emploi et l’obtenir, accomplir les préparatifs nécessaires et débuter la tâche demandée. 
> Vous devez dépenser 4 jours de temps libre pour accomplir une tâche de votre niveau. Réduisez le nombre d’une journée pour chaque niveau de la tâche en dessous de la vôtre avec un minimum de 1 jour. À l’inverse, ajoutez un jour de temps supplémentaire pour chaque niveau de la tâche supérieur au vôtre.
> Après avoir dépensé ce nombre de journées de temps libre, vous obtenez le nombre initial des gains et vous pouvez continuer à accomplir cette tâche pour en obtenir davantage.
> L’entrée réussite vous explique comment cela fonctionne.
> Notez que si vous souhaitez accepter une tâche qui ne nécessite qu’une journée de temps libre, il vous faut accepter une tâche qui ne nécessitera qu’une journée de préparation.
> Une tâche de niveau 0 nécessite toujours une journée de réparation.
> Après avoir passé le nombre de jours de base de temps libre nécessaire pour commencer votre tâche, vous effectuez votre test de Savoir pour déterminer vos gains.
Vous continuez à gagner ce montant chaque jour suivant pendant la durée de votre emploi, sans qu’il soit nécessaire de réaliser d’autres tests.
> Le MJ détermine combien de temps dure l’emploi en fonction du nombre de jours que nécessite la tâche et d’autres facteurs.
> La plupart des tâches durent une ou deux semaines, bien que certaines puissent prendre des mois ou même des années.
> Si vous abandonnez une tâche au milieu, il vous faudra ensuite retrouver une autre tâche à votre retour mais le MJ peut décider de vous permettre de la récupérer à votre retour là où vous en étiez (ce qui ne survient habituellement que si la tâche n’est pas terminée et que votre installation est toujours fonctionnelle. 
>
> **Réussite** Vous faites un bon travail et obtenez le montant des gains listé sur la table ci-après Revenus de la compétence pour le niveau de la tâche donnée en fonction de votre degré de maîtrise.
>
> **Réussite** critique Vous excellez dans votre tâche. Comme avec une réussite, mais vous obtenez les gains de la tâche correspondant au niveau supérieur. Le MJ peut augmenter la durée pendant laquelle vous pouvez rester employé à cette tâche.
>
> **Échec critique** Vous ne gagnez rien pour votre travail et êtes licencié immédiatement. Votre réputation souffre, vous rendant potentiellement difficile de trouver des emplois gratifiants dans cette communauté dans le futur.

{: .tabletitle}
Revenu de la compétence

{: .table .table-striped .table-sm .table-hover}
| Niveau de la tâche | Échec | Entraîné | Expert | Maître | Légendaire
| 0 | 1 pc | 5 pc | 5 pc | 5 pc | 5 pc
| 1 | 2 pc | 1 pa | 1 pa | 1 pa | 1 pa
| 2 | 4 pc | 2 pa | 2 pa | 2 pa | 2 pa
| 3 | 8 pc | 4 pa | 4 pa | 4 pa | 4 pa
| 4 | 1 pa | 5 pa | 6 pa | 6 pa | 6 pa
| 5 | 2 pa | 9 pa | 10 pa | 10 pa | 10 pa
| 6 | 2 pa | 12 pa | 14 pa | 14 pa | 14 pa
| 7 | 3 pa | 16 pa | 20 pa | 20 pa | 20 pa
| 8 | 4 pa | 20 pa | 28 pa | 30 pa | 30 pa
| 9 | 5 pa | 25 pa | 36 pa | 40 pa | 40 pa
| 10 | 6 pa | 30 pa | 45 pa | 50 pa | 50 pa
| 11 | 7 pa | 35 pa | 60 pa | 75 pa | 75 pa
| 12 | 8 pa | 40 pa | 75 pa | 100 pa | 100 pa
| 13 | 10 pa | 50 pa | 100 pa | 150 pa | 150 pa
| 14 | 15 pa | 75 pa | 150 pa | 250 pa | 250 pa
| 15 | 20 pa | 100 pa | 200 pa | 350 pa | 350 pa
| 16 | 25 pa | 125 pa | 250 pa | 475 pa | 500 pa
| 17 | 30 pa | 150 pa | 300 pa | 650 pa | 700 pa
| 18 | 40 pa | 200 pa | 450 pa | 900 pa | 1.000 pa
| 19 | 60 pa | 300 pa | 600 pa | 1.200 pa | 1.500 pa
| 20 | 75 pa | 375 pa | 750 pa | 1.500 pa | 2.000 pa
| 20 (crit.) | 90 pa | 450 pa | 900 pa | 1.750 pa | 2.500 pa